![coverage](https://gitlab.com/bruneelBehra/projetdevops/badges/main/coverage.svg?min_good=80)

# ProjetDevOps

Projet finale du module DevOps, Master Génie Informatique première année.

## Fonctionnalités
La librairie permet de travailler sur deux classes :
- DataFrame <Integer, ArrayList<Column>> : une map permettant de gérer les colonnes et les lignes à la fois
- Column, identifié par un label unique parmi toutes les colonnes, et contenant une liste de valeurs de type Object.

La classe Dataframe vous permet de :
- Sélectionner une ou plusieurs lignes renvoyant dans tous les cas un nouveau dataframe contenant les lignes sélectionnées avec toutes les colonnes
- Sélectionner des colonnes selon leur nom, renvoyent une liste de colonne
- Copier une partie du dataframe, en ne prenant que certaines colonnes et / ou certaines lignes
- Sélectionner les lignes dont la valeur d'une colonne souhaitée est comprise entre des bornes MIN et MAX.
- Comparer l'égalité des colonnes, lignes et données avec un autre Dataframe
- La métohde toString() permettra d'afficher correctement les dataframes.

La classe Column vous permet de :
- Récupérer toutes les valeurs d'une colonne
- Obtenir le Min, le Max et la moyenne des valeurs sur les colonnes de type INT ou FLOAT
- Obtenir le nombre d'occurence d'une certaine valeur dans la colonne
- Copier la colonne et trier la copie par ordre croissant ou décroissant
- Comparer l'égalité des valeurs et de l'identifiant avec une autre colonne
- La méthode toString() permettra d'afficher correctement les colonnes

## Outils
Pour réaliser ce projet nous avons utiliser les outils suivants :
- JUnit pour les tests unitaires
- Maven pour la configuration du CI
- Maven-site / Maven javadoc pour configurer le GitLab Pages
- Jacoco pour le code coverage

## Workflow
Le repo git comportait une branche 'main' servant de branche stable.
Pour chaque nouvelle version, on crée un tag avec une release associée. La dernière release est indiquée par un badge.
Chaque ajout de fonctionnalités / code / CI/CD se faisait sur une nouvelle branche.
git rebase pour tout retard de code à implémenter sur la branche de travail.
Merge Request pour chaque merge et avancement des branches (permet de voir les pipelines plus facilement)
Les merges étaient validés par un autre développeur via la MR.

## Feedback
Le projet était cool, très intéressant sur la manière d'utiliser des outils réellement pratique pour une insertion professionnelle.

## Documentation
La documentation de la librairie est accessible sur le lien GitLab Pages :
https://bruneelbehra.gitlab.io/-/projetdevops/-/jobs/2382711850/artifacts/index.html

## Authors
Projet réalisé par :
- Yasser Akhsass
- Elliot Bruneel
- Fabien Behra
