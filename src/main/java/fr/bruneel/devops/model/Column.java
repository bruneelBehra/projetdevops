package fr.bruneel.devops.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Column possède un identifiant et une liste de données
 */
public class Column {
    private String label = "";
    private EType dataType = EType.UNKNOWN;
    private ArrayList<Object> oData = new ArrayList<>();

    /**
     * Retourne le label de la colonne
     * @return Label de la colonne
     */
    public String getLabel(){return label;}

    /**
     * Retourne le datatype de la colonne
     * @return EType, BOOL, STRING, FLOAT ou INT en fonction du type de données du tableau
     */
    public EType getType(){return this.dataType;}

    /**
     * Retourne la première valeur de la colonne
     * @return Première valeur de la colonne
     */
    public Object getValue(){return oData.get(0);}

    /**
     * Retournes les valeurs
     * @return Valeurs de la colonne
     */
    public ArrayList<Object> getValues() {return oData;}

    /**
     * Construit la colonne avec le label indiqué
     * @param label Identifiant de la colonne
     */
    public Column(String label){
        this.label = label;
    }

    /**
     * Construit la colonne avec le label indiqué et les valeurs données
     * @param label Identifiant de la colonne
     * @param value Valeur de la colonne
     * @throws Exception Si erreur dans le cast des valeurs
     */
    public Column(String label, Object value) throws Exception {
        this(label,new ArrayList<Object>(Arrays.asList(value)));
    }

    /**
     * Construit la colonne avec le label indiqué et les valeurs données
     * @param label Identifiant de la colonne
     * @param values Valeurs de la colonne
     * @throws Exception Si erreur dans le cast des valeurs
     */
    public Column(String label, Object[] values) throws Exception {
        this(label, new ArrayList<Object>(Arrays.asList(values)));
    }

    /**
     * Construit la colonne avec le label indiqué et les valeurs données
     * @param label Identifiant de la colonne
     * @param arl Valeurs de la colonne
     * @throws Exception Si erreur dans le cast des valeurs
     */
    public Column(String label, ArrayList<Object> arl) throws Exception {
        this.label = label;

        if (arl != null && arl.size() > 0)
        {
            if (this.dataType == EType.UNKNOWN){
                this.dataType = this.processType(arl.get(0));
            }

            switch(dataType){
                case BOOL:
                for (int i = 0; i < arl.size(); i++){
                    try{
                        oData.add(Boolean.parseBoolean(arl.get(i).toString()));
                    }
                    catch(ClassCastException cce){
                        throw new Exception(String.format("Cast impossible, type demandé : %s. Colonne %s, Value %s", this.dataType.toString(), this.label, arl.get(i)));
                    }
                }
                break;
                case INT:
                for (int i = 0; i < arl.size(); i++){
                    try{
                        oData.add(Integer.parseInt(arl.get(i).toString()));
                    }
                    catch(ClassCastException cce){
                        throw new Exception(String.format("Cast impossible, type demandé : %s. Colonne %s, Value %s", this.dataType.toString(), this.label, arl.get(i)));
                    }
                }
                break;
                case FLOAT:
                for (int i = 0; i < arl.size(); i++){
                    try{
                        oData.add(Float.parseFloat(arl.get(i).toString()));
                    }
                    catch(ClassCastException cce){
                        throw new Exception(String.format("Cast impossible, type demandé : %s. Colonne %s, Value %s", this.dataType.toString(), this.label, arl.get(i)));
                    }
                }
                break;
                default:
                for (int i = 0; i < arl.size(); i++){
                    oData.add(arl.get(i));
                }
                break;
            }
        }
    }

    /**
     * Récupère la valeur maximale contenue dans la colonne
     * @return Float, valeur max.
     */
    public Float getMaxValue()
    {
        if (this.dataType == EType.BOOL || this.dataType == EType.STRING)
        return null;
        
        Float max = Float.parseFloat(this.oData.get(0).toString());
        for(int i = 1; i < this.oData.size(); i++){
            Float fvalue = Float.parseFloat(this.oData.get(i).toString());
            if (max <= fvalue)
                max = fvalue;
        }

        return max;
    }

    /**
     * Récupère la valeur minimale contenue dans la colonne
     * @return Float, valeur min.
     */
    public Float getMinValue()
    {
        if (this.dataType == EType.BOOL || this.dataType == EType.STRING)
        return null;
        
        Float min = Float.parseFloat(this.oData.get(0).toString());
        for(int i = 1; i < this.oData.size(); i++){
            Float fvalue = Float.parseFloat(this.oData.get(i).toString());
            if (min >= fvalue)
                min = fvalue;
        }

        return min;
    }

    /**
     * Récupère la moyenne des valeurs de la colonne
     * @return Float, valeur moyenne
     */
    public Float getAverageValue()
    {
        if (this.dataType == EType.BOOL || this.dataType == EType.STRING || this.dataType == EType.UNKNOWN)
            return null;
        
        Float sum = Float.parseFloat(this.oData.get(0).toString());
        for(int i = 1; i < this.oData.size(); i++){
            Float fvalue = Float.parseFloat(this.oData.get(i).toString());
            sum += fvalue;
        }

        return sum/this.oData.size();
    }

    /**
     * Retourne le nombre d'éléments dans la colonne
     * @return Int, nombre d'éléments
     */
    public int size(){
        return this.oData.size();
    }

    /**
     * Vrai si la colonne ne comporte aucune donnée, faux sinon
     * @return Bool, vrai si vide, faux sinon
     */
    public Boolean isEmpty(){
        return this.oData.size() == 0;
    }

    /**
     * Récupère la première valeur de la colonne
     * @return Object première valeur
     */
    public Object first(){
        if (this.oData.size() == 0) return null;
        return this.oData.get(0);
    }

    /**
     * Récupère la dernière valeur de la colonne
     * @return Object première valeur
     */
    public Object last(){
        if (this.oData.size() == 0) return null;
        return this.oData.get(this.oData.size() - 1);
    }

    /**
     * Retourne le nombre d'occurence de la valeur dans la colonne
     * @param value Valeur dont on veut les occurences
     * @return Nombre d'occurences de la valeur
     */
    public int getOccurences(Object value)
    {
        if (value == null) return 0;
        if (this.isEmpty()) return 0;

        int i = 0;
        for (Object o : this.oData){
            if (this.dataType == EType.STRING && o.toString().equals(value.toString())){
                i++;
            }
            else if (o.equals(value)){
                i++;
            } 
        }

        return i;
    }

    /**
     * Retourne une colonne avec les valeurs triées par ordre croissant
     * @return Column avec valeurs triées ASC
     */
    public Column orderByAscending(){
        try{
            if (this.oData.isEmpty() || this.oData.size() == 1) return new Column(this.label, this.oData);
            Column col = new Column(this.label, this.oData);
            Collections.sort(col.oData, new ValueComparator());
            return col;
        }
        catch (Exception ex){
            return null;
        }
    }

    /**
     * Retourne une colonne avec les valeurs triées par ordre croissant
     * @return Column avec valeurs triées DESC
     */
    public Column orderByDescending(){
        try{
            if (this.oData.isEmpty() || this.oData.size() == 1) return new Column(this.label, this.oData);
            Column col = new Column(this.label, this.oData);
            Collections.sort(col.oData, new ValueDescComparator());
            return col;
        }
        catch (Exception ex){
            return null;
        }
    }

    @Override
    public String toString(){

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < oData.size(); i++){
            String s = "| "+ oData.get(i).toString() + "|\n";
            str.append(s);
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o){
        if (o == this) return true;
        if (!(o instanceof Column)) return false;

        Column c = (Column)o;
        if (c.getLabel().equals(this.label) == false){
            System.out.println("LABEL : "+c.getLabel()+ " : "+label);
            return false;
        }
        if (c.getValues().size() != this.getValues().size()){
            System.out.println("SIZE : "+c.getValues().size()+ " : "+this.getValues().size());
            return false;
        }

        for(int i = 0; i < c.getValues().size(); i++){
            if (c.getValues().get(i).equals(this.getValues().get(i)) == false) {
                System.out.println("VALUE : "+c.getValues().get(i)+ " : "+this.getValues().get(i));
                return false;
            }
        }
        return true;
    }

    /**
     * Ajoute la value à la première ligne de la colonne.
     * @param value Valeur à ajouter
     */
    public void addValue(Object value)
    {
        if (this.dataType == EType.UNKNOWN)
            this.dataType = this.processType(value);
            
        oData.add(value);
    }

    /**
     * Défini le type de la valeur
     * @param value 
     * @return EType, type de la valeur
     */
    private EType processType(Object value)
    {
        EType returnType;
        try {
            Integer.parseInt(value.toString());
            returnType = EType.INT;
        } catch (Exception e) {
            try{
                Float.parseFloat(value.toString());
                returnType = EType.FLOAT;
            }
            catch(Exception e1){
                try{
                    if (isBoolean(value))
                    {
                        returnType = EType.BOOL;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch(Exception e2){
                    returnType = EType.STRING;
                }
            }
        }
        return returnType;
    }

    /**
     * Retourn vrai si value peut être parse en bool
     * @param value String à parser
     * @return Vrai si value est un booleen, faux sinon.
     */
    private Boolean isBoolean(Object value) {
        return value != null && Arrays.stream(new String[]{"true", "false", "1", "0"})
                .anyMatch(b -> b.equalsIgnoreCase(value.toString()));
    }
}
