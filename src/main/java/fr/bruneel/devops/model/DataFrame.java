package fr.bruneel.devops.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import com.opencsv.CSVReader;

/**
 * Dataframe contient une map Int ID, ArrayList de Column Cols
 * Les noms des colonnes sont dans columnIdentifiers
 */
public class DataFrame {

    private final LinkedHashMap<Integer, ArrayList<Column>> map = new LinkedHashMap<Integer, ArrayList<Column>>();
    private final ArrayList<String> columnsIdentifiers = new ArrayList<>();

    /**
     * Créer un dataframe vide.
     */
    public DataFrame(){}

    /**
     * Créer un Dataframe à partir d'un fichier .csv
     * @param csvFileName Nom du Fichier csv
     */
    public DataFrame(String csvFileName) 
    {
        if (csvFileName == null || csvFileName == "")
            throw new IllegalArgumentException("le chemin ne peut pas être nul ou vide");

        if (!csvFileName.endsWith(".csv"))
            throw new IllegalArgumentException("Format du fichier interdit, seul .csv autorisé.");
        String basePath = new File("").getAbsolutePath();

        File f = new File(basePath+"/src/data/" + csvFileName);
        if (!f.exists()){
            f = new File(basePath+"/projetdevops/src/data/"+csvFileName);
            if(!f.exists())
                throw new IllegalArgumentException("Le fichier n'existe pas");
        }

        try {
            CSVReader reader = new CSVReader(new BufferedReader(new FileReader(f)) );
            List<String[]> r = reader.readAll();
            this.populateMap(r.toArray(new String[r.size()][]));
        }
        catch(Exception ex)
        {
            throw new IllegalArgumentException("Erreur lors de la lecture du fichier.");
        }
    }

    /**
     * Créer un dataframe à partir d'un tableau 2D de données
     * @param data Tableau 2D de données
     */
    public DataFrame(Object[][] data)
    {
        if (data == null)
            throw new IllegalArgumentException("data ne peut être nul");

        this.populateMap(data);
    }

    /**
     * Créer un dataframe à partir d'une map
     * @param map Map de données
     */
    private DataFrame(LinkedHashMap<Integer, ArrayList<Column>> map)
    {
        if (map == null)
            throw new IllegalArgumentException("data ne peut être nul");

        this.map.clear();
        this.map.putAll(map);
    }

    /**
     * Retourne les identifiants des colonnes
     * @return Identifiants des colonnes
     */
    public ArrayList<String> getColumnIdentifiers()
    {
        if (this.columnsIdentifiers.size() == 0){
            if (this.map.size() == 0) return null;
            Entry<Integer, ArrayList<Column>> firstEntry = map.entrySet().iterator().next();
            for(int i = 0; i < firstEntry.getValue().size(); i++){
                this.columnsIdentifiers.add(firstEntry.getValue().get(i).getLabel());
            }
        }
        return this.columnsIdentifiers;
    }

    //region GetRows
    /**
     * Récupère les lignes et leurs valeurs dans la range indiquée
     * @param startIndex Index de départ
     * @param count Nombre de ligne à récupérer
     * @return DataFrame contenant les lignes demandées
     */
    public DataFrame getRows(int startIndex, int count)
    {
        if (startIndex != Math.max(0, Math.min(startIndex, map.size()-1))) throw new IndexOutOfBoundsException("startIndex doit être compris entre 0 et "+ (map.size()-1));
        if (count != Math.max(1, Math.min(count, map.size()))) throw new IndexOutOfBoundsException("count doit être compris entre 1 et "+ (map.size()));
        LinkedHashMap<Integer, ArrayList<Column>> map = new LinkedHashMap<>();

        for(int i = startIndex; i < startIndex + count; i++){
            map.put(i, this.map.get(i));
        }

        DataFrame dataFrame = new DataFrame(map);
        return dataFrame;
    }

    /**
     * Récupère les X première lignes, X étant count
     * @param count nombre de lignes à récupérer
     * @return DataFrame contenant les lignes demandées
     */
    public DataFrame getFirstRows(int count){
        return this.getRows(0, count);
    }

    /**
     * Récupère les X dernières lignes, X étant count
     * @param count nombre de lignes à récupérer
     * @return DataFrame contenant les lignes demandées
     */
    public DataFrame getLastRows(int count){
        Set<Integer> keys = this.map.keySet();
        int startIndex = keys.toArray(new Integer[keys.size()])[this.map.size()-count];
        return this.getRows(startIndex, count);
    }

    /**
     * Get column by identifier
     * @param identifier Id de la colonne
     * @return Colonne qui a comme Label identifier
     */
    public Column getColumn(String identifier){
        if (map.isEmpty()) { return null; }
        if (this.columnsIdentifiers.contains(identifier) == false){ throw new IllegalArgumentException(identifier + " n'est pas reconnu comme identifiant de colonne.");}

        Column col = new Column(identifier);
        int colIndex = this.getColumnIdentifiers().indexOf(identifier);
        for (Entry<Integer, ArrayList<Column>> entry : map.entrySet()) {
            col.addValue(entry.getValue().get(colIndex).getValue());
        }

        return col;
    }

    /**
     * Créer une copie du dataframe avec les colonnes demandées
     * @param identifiers Ids des colonnes souhaitées
     * @return DataFrame avec les colonnes souhaitées
     */
    public DataFrame copyPartColumns(String[] identifiers){
        if (identifiers == null || identifiers.length == 0) return null;
        List<Integer> identifiersIndex = new ArrayList<>();
        LinkedHashMap map = new LinkedHashMap<>();

        //récupération des index
        for(String id : identifiers){
            int index = this.getColumnIdentifiers().indexOf(id);
            if (index >= 0)
                 identifiersIndex.add(index);
            else
                throw new IllegalArgumentException("un des identifiers ne correspond pas à une colonne");
        }

        for(Entry<Integer, ArrayList<Column>> entry : this.map.entrySet()){
            ArrayList<Column> cols = new ArrayList<>();
            for(int i = 0; i < identifiersIndex.size(); i++){
                cols.add(entry.getValue().get(identifiersIndex.get(i)));
            }
            map.put(entry.getKey(), cols);
        }

        return new DataFrame(map);
    }

    /**
     * Créer une copie du dataframe avec les lignes demandées
     * @param identifiers Ids des lignes souhaitées
     * @return DataFrame avec les lignes souhaitées
     */
    public DataFrame copyPartRows(Integer[] identifiers){
        if (identifiers == null || identifiers.length == 0) return null;
        LinkedHashMap map = new LinkedHashMap<>();

        for(Integer index : identifiers){
            if (this.map.containsKey(index) == false)
                throw new IllegalArgumentException("la ligne "+index+" n'est pas présent dans le dataframe");
            map.put(index, this.map.get(index));
        }

        return new DataFrame(map);
    }

    /**
     * Créer une copie du dataframe avec les colonnes et les lignes demandées
     * @param rowIds Ids des lignes souhaitées
     * @param colIds Ids des colonnes souhaitées
     * @return DataFrame avec les lignes et colonnes souhaitées.
     */
    public DataFrame copyPart(Integer[] rowIds, String[] colIds){
        return this.copyPartRows(rowIds).copyPartColumns(colIds);
    }

    /**
     * Créer un DataFrame avec les valeurs comprises entre les bornes
     * @param label Colonne à récupérer
     * @param min Borne Min
     * @param max Borne Max
     * @return Dataframe avec les valeurs comprises entre les bornes
     */
    public DataFrame select(String label, float min, float max){
        Integer id = 0;
        for(String s: this.getColumnIdentifiers()){
            if(s.equals(label)){
                id = this.getColumnIdentifiers().indexOf(s);
            }
        }
        if(!this.map.get(0).get(id).getType().equals(EType.FLOAT) && !this.map.get(0).get(id).getType().equals(EType.INT))
            throw new IllegalArgumentException("Cette structure de données est inquantifiable");

        LinkedHashMap<Integer, ArrayList<Column>> map = new LinkedHashMap<>();
        for(int i = 1; i < this.map.size(); i++){
            float value = Float.parseFloat(this.map.get(i).get(id).getValue().toString());
            if(value <= max && value >= min)
                map.put(i, this.map.get(i));
        }
        DataFrame ret = new DataFrame(map);
        return ret;
    }

    /**
     * Créer un DataFrame avec les lignes qui contiennent la valeur
     * @param label Colonne à récupérer
     * @param value valeur à comparer
     * @return Dataframe avec les valeurs comprises entre les bornes
     */
    public DataFrame select(String label, String value){
        Integer id = 0;
        for(String s: this.getColumnIdentifiers()){
            if(s.equals(label)){
                id = this.getColumnIdentifiers().indexOf(s);
            }
        }
        if(this.map.get(0).get(id).getType().equals(EType.FLOAT) || this.map.get(0).get(id).getType().equals(EType.INT))
            throw new IllegalArgumentException("Cette structure de données est quantifiable");
            
        LinkedHashMap<Integer, ArrayList<Column>> map = new LinkedHashMap<>();
        for(int i = 0; i < this.map.size(); i++){

            String current = this.map.get(i).get(id).toString().replaceAll("[| `\n]", "");
            if(current.equals(value))
                map.put(i, this.map.get(i));
        }
        DataFrame ret = new DataFrame(map);
        return ret;
    }

    /**
     * Peuple la map de données
     * @param data
     */
    private void populateMap(Object[][] data){
        for(int i = 0; i < data.length; i++)
        {
            ArrayList<Column> columns = new ArrayList<>();
            //la première ligne indique le nom des colonnes
            if (i == 0)
                for(int j = 0; j < data[i].length; j++){
                    columnsIdentifiers.add(data[i][j].toString());
                }

            // la deuxième permet de définir le type
            if (i > 0){
                for (int j = 0; j < data[i].length; j++){
                    try
                    {
                        Column col = new Column(columnsIdentifiers.get(j).toString(), new String[] {data[i][j].toString()});
                        columns.add(col);
                    }
                    catch(Exception ex){
                        System.out.println(ex.getMessage());
                        return;
                    }                    
                }
                map.put(i-1, columns);
            }
        };
    }

    @Override
    public String toString(){

        StringBuilder str = new StringBuilder();
        str.append("--------------------------------------------------------------------------------------------------------\n");
        for(Entry<Integer, ArrayList<Column>> entry : map.entrySet()){
            str.append("KEY_" + entry.getKey());
            for(Column col : entry.getValue()){
                String s = "|" + col.getValue().toString() + ((entry.getValue().get(entry.getValue().size()-1).getLabel() == col.getLabel()) ? "|\n" : "");
                str.append(s);
            }
            str.append("--------------------------------------------------------------------------------------------------------\n");
        }

        return str.toString();
    }

    @Override
    public boolean equals(Object o){
        if (o == this) return true;
        if (!(o instanceof DataFrame)) return false;

        DataFrame df = (DataFrame)o;
        //Comparaison entre le nombre de lignes
        if (df.map.size() != this.map.size())  return false;

        //Comparaison entre les colonnes
        if (!df.getColumnIdentifiers().equals(this.getColumnIdentifiers())) return false;

        try{

            Iterator<ArrayList<Column>> iterator = this.map.values().iterator();
            int count =0;
            for(Iterator<ArrayList<Column>> dfIterator = df.map.values().iterator(); dfIterator.hasNext();){
                ArrayList<Column> expectedColsInARow = iterator.next();
                ArrayList<Column> dfColsInARow = dfIterator.next();
                for(int i = 0; i < dfColsInARow.size(); i++){
                    if (!expectedColsInARow.get(i).equals(dfColsInARow.get(i))){
                        System.out.println("Row : "+count+" Colonne : "+expectedColsInARow.get(i).getLabel() +" Valeur : "+expectedColsInARow.get(i).getValue());
                        return false;
                    };
                }
                count++;
            }

            return true;
        }
        catch (Exception ex){
            return false;
        }
    }
}
