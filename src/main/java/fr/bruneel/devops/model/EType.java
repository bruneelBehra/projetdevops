package fr.bruneel.devops.model;

/**
 * Type de données
 */
public enum EType {
    /**
     * Type String
     */
    STRING, 
    /**
     * Type Int
     */
    INT, 
    /**
     * Type Float
     */
    FLOAT, 
    /**
     * Type Bool
     */
    BOOL, 
    /**
     * Type Unknown
     */
    UNKNOWN
}
