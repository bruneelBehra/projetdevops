package fr.bruneel.devops.model;

public class Main {
    
    public static void main (String[] args){

        //DataFrame data = new DataFrame("file.csv");
        //System.out.print(data.toString());

        try{

            // Column col = data.getColumn("age");
            // Column col1 = data.getColumn("first_name");
            // //System.out.println(col.getMaxValue());
            // //System.out.println(col.getMinValue());
            // //System.out.println(col1.getAverageValue());

            // System.out.println(data.copyFromColumns(new String[]{"age", "first_name","address"}).toString());
            // System.out.println(data.copyFromRows(new Integer[]{2,4,6,9}).toString());

            // // System.out.println(data.getRows(0, 10, false).toString());
            //  System.out.println(data.getFirstRows(10).toString());
            // // System.out.println(data.getRow²s(9, 5, true).toString());
            //  System.out.println(data.getLastRows(5).toString());

             //Test Dataframe depuis csv
            DataFrame test1 = new DataFrame("file.csv");

            System.out.println(test1.getColumn("first_name").orderByAscending().toString());

            //Test Dataframe depuis tableau de données
            // Object[][] dataSet = new Object[][]{
            //     {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            //     {1,"Daria","Crammy","dcrammy0@dailymail.co.uk",1,true,15.71f},
            //     {2,"Aleen","Childers","achilders1@linkedin.com",2,false,6.73f},
            //     {3,"Ulberto","Kubu","ukubu2@house.gov",3,true,16.59f},
            //     {4,"Tremain","Insley","tinsley3@cam.ac.uk",4,true,15.43f},
            //     {5,"Darda","Mogenot","dmogenot4@constantcontact.com",5,false,7.87}
            // };
            // DataFrame test2 = new DataFrame(dataSet);

            // //Comparaison entre les deux DataFrame
            // DataFrame dataFrameFile = new DataFrame("file.csv");
            // DataFrame dataFrameDataSet = new DataFrame(dataSet);

            // System.out.println(dataFrameFile.getFirstRows(5).toString());
            // System.out.println(dataFrameDataSet.toString());

            // System.out.println(dataFrameFile.getFirstRows(5).equals(dataFrameDataSet));

            //System.out.println(test1.select("age", 10, 50));

            // System.out.println(data.getRows(0, 10, false).toString());
            // System.out.println(data.getRows(3, 4, false).toString());
            // System.out.println(data.getRows(7, 3, true).toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            //System.out.println("Opération illégale pour cette structure de données");
        }

        //data.getColumns();
    }

    //     //r.forEach(x -> System.out.println(Arrays.toString(x)));
    //     System.out.println("******************");
    //     //System.out.println(r.get(0)[1]);
    //     System.out.println("*************");
        
    //     for(String s: r.get(0)){
    //         System.out.println(s);
    //     }  
        
    //     System.out.println("*************");
    //     System.out.println("*************");
    //     System.out.println("*************");
    //     Object obj = DataFrame.processType(r.get(1)[3]);
    //     if(obj instanceof Integer){
    //         System.out.println("Integer");
    //     }
    //     if(obj instanceof Float){
    //         System.out.println("Float");
    //     }
    //     if(obj instanceof String){
    //         System.out.println("String");
    //     }
    // }
}
