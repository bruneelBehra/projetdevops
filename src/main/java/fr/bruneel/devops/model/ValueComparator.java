package fr.bruneel.devops.model;

import java.util.Comparator;

/**
 * Compare la valeur de deux objet de manière croissante
 */
public class ValueComparator implements Comparator<Object>{

    /**
     * Créer un ValueComparator
     */
    public ValueComparator(){}

    /**
     * @param arg0 objet comparable
     * @param arg1 objet à comparer
     * @return 1,0,-1 en fonction de la comparaison
     */
    @Override
    public int compare(Object arg0, Object arg1) {
        if (arg0 instanceof Integer && arg1 instanceof Integer)
            return Integer.compare((int)arg0, (int)arg1);

        if (arg0 instanceof Float && arg1 instanceof Float) 
            return Float.compare((Float)arg0, (Float)arg1);

        if (arg0 instanceof Boolean && arg1 instanceof Boolean) 
            return Boolean.compare((Boolean)arg0, (Boolean)arg1);

        if (arg0 instanceof String && arg1 instanceof String) 
            return ((String)arg0).compareTo((String)arg1);

        return 0;
    }
    
}
