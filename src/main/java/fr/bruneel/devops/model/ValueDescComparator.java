package fr.bruneel.devops.model;

import java.util.Comparator;

/**
 * Compare la valeur de deux objet de manière décroissante
 */
public class ValueDescComparator implements Comparator<Object>{

    /**
     * Créer un ValueDescComparator
     */
    public ValueDescComparator(){}

    /**
     * @param arg0 objet comparable
     * @param arg1 objet à comparer
     * @return 1,0,-1 en fonction de la comparaison
     */
    @Override
    public int compare(Object arg0, Object arg1) {
        if (arg0 instanceof Integer && arg1 instanceof Integer)
            return -1*Integer.compare((int)arg0, (int)arg1);

        if (arg0 instanceof Float && arg1 instanceof Float) 
            return -1*Float.compare((Float)arg0, (Float)arg1);

        if (arg0 instanceof Boolean && arg1 instanceof Boolean) 
            return -1*Boolean.compare((Boolean)arg0, (Boolean)arg1);

        if (arg0 instanceof String && arg1 instanceof String) 
            return -1*((String)arg0).compareTo((String)arg1);

        return 0;
    }
    
}
