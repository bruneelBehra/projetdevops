package fr.bruneel.devops.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import javax.xml.crypto.Data;

class DataFrameTest {

    private final Object[][] fiveFirstRowsDataSet = new Object[][]{
        {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
        {1,"Padriac","M'Quharge","pmquharge0@google.com.hk",74,true,5.8f},
        {2,"Joella","Larwell","jlarwell1@youku.com",33,true,15.28f},
        {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
        {4,"Adela","Eagle","aeagle3@huffingtonpost.com",18,false,11.19f},
        {5,"Jane","Michelle","jmichelle4@aol.com",26,false,5.19f}
    };

    /**
     * Test les différentes manière de créer et d'instancier les colonnes
     */
    @Test
    void instanceDataFrame(){
        //check nullité, fichier non présent
        assertThrows(IllegalArgumentException.class, () -> {DataFrame df = new DataFrame("C:/Path/qui/nexiste/pas.csv");});
        assertThrows(IllegalArgumentException.class, () -> {DataFrame df = new DataFrame("file.scv");});
        assertThrows(IllegalArgumentException.class, () -> {DataFrame df = new DataFrame("");});
        
        //Test Dataframe depuis csv
        assertDoesNotThrow(() -> { 
            DataFrame dataFrame = new DataFrame("file.csv");
        });

        //Test Dataframe depuis tableau de données
        assertDoesNotThrow(() -> { 
            DataFrame dataFrame = new DataFrame(fiveFirstRowsDataSet);
        });

        //Comparaison entre les deux DataFrame
        DataFrame dataFrameFile = new DataFrame("file.csv");
        DataFrame dataFrameDataSet = new DataFrame(fiveFirstRowsDataSet);

        assertEquals(dataFrameFile.getFirstRows(5), dataFrameDataSet);
    }

    /**
     * Test les différentes manières de récupérer certaines lignes de la dataFrame
     */
    @Test
    void getRows(){
        final Object[][] expectedDataSet = new Object[][]{
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {2,"Joella","Larwell","jlarwell1@youku.com",33,true,15.28f},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
            {4,"Adela","Eagle","aeagle3@huffingtonpost.com",18,false,11.19f},
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);

        //gestion des cas exceptions
        DataFrame df = new DataFrame(fiveFirstRowsDataSet);
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getRows(-1, 3);});
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getRows(0, 6);});

        assertEquals(expectedDf, df.getRows(1, 3));
    }

    /**
     * Test les différentes manières de récupérer les premières lignes de la dataFrame
     */
    @Test
    void getFirstRows(){
        final Object[][] expectedDataSet = {
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {1,"Padriac","M'Quharge","pmquharge0@google.com.hk",74,true,5.8f},
            {2,"Joella","Larwell","jlarwell1@youku.com",33,true,15.28f},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);

        DataFrame df = new DataFrame(fiveFirstRowsDataSet);
        DataFrame res = df.getFirstRows(3);
        assertTrue(res.equals(expectedDf));
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getFirstRows(0);});
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getFirstRows(fiveFirstRowsDataSet.length);});
    }

    /**
     * Test les différentes manières de récupérer les dernières lignes de la dataFrame
     */
    @Test
    void getLastRows(){
        final Object[][] expectedDataSet = {
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
            {4,"Adela","Eagle","aeagle3@huffingtonpost.com",18,false,11.19f},
            {5,"Jane","Michelle","jmichelle4@aol.com",26,false,5.19f}
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);

        DataFrame df = new DataFrame(fiveFirstRowsDataSet);
        DataFrame res = df.getLastRows(3);
        assertTrue(res.equals(expectedDf));

        //tests sur exception
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getLastRows(0);});
        assertThrows(IndexOutOfBoundsException.class, () -> {df.getLastRows(fiveFirstRowsDataSet.length);});
    }

    /**
     * Test la récupération d'une colonne par son identifiant
     */
    @Test
    void getColumn(){
        final DataFrame df = new DataFrame("file.csv");
     
        //test GetColumn(Identifiant) sur colonne non existante
        assertThrows(IllegalArgumentException.class, () -> df.getColumn(""));
        assertThrows(IllegalArgumentException.class, () -> df.getColumn(null));
        assertThrows(IllegalArgumentException.class, () -> df.getColumn("FIRST_NAME"));
    
        //Test GetColumn(Identifiant) sur dataframe vide
        DataFrame emptyDf = new DataFrame();
        assertEquals(null, emptyDf.getColumn("age"));

        //Test GetColumn(Identifiant) avec bonnes infos
        Column colFirstName = df.getColumn("first_name");
        assertEquals(1000, colFirstName.size());
        assertEquals("Padriac", colFirstName.getValues().get(0));
        assertEquals("Padriac", colFirstName.first());
        assertEquals("Nappy", colFirstName.getValues().get(586));
        assertEquals("Cristina", colFirstName.getValues().get(999));
        assertEquals("Cristina", colFirstName.last());
    }

    /**
     * Test les différentes statistiques de base sur la dataFrame, Moyenne, Min, Max.
     */
    @Test
    void getMinMaxAverage(){
        final DataFrame df = new DataFrame("file.csv");

        assertEquals(1f, df.getColumn("age").getMinValue());
        assertEquals(50.84f, df.getColumn("age").getAverageValue());
        assertEquals(100f, df.getColumn("age").getMaxValue());
        assertEquals(1.02f, df.getColumn("Note au BAC").getMinValue());
        assertEquals(10.359098f, df.getColumn("Note au BAC").getAverageValue());
        assertEquals(19.98f, df.getColumn("Note au BAC").getMaxValue());

        //Tests stats sur colonnes interdites
        assertEquals(null, df.getColumn("first_name").getAverageValue());
        assertEquals(null, df.getColumn("first_name").getMinValue());
        assertEquals(null, df.getColumn("first_name").getMaxValue());
    }

    /**
     * Test le calcul d'occurences
     */
    @Test
    void getOccurences(){
        final DataFrame df = new DataFrame("file.csv");

        assertEquals(1, df.getColumn("first_name").getOccurences("Saleem"));
        assertEquals(15, df.getColumn("age").getOccurences(25));
        assertEquals(505, df.getColumn("heureux?").getOccurences(true));
        assertEquals(0, df.getColumn("last_name").getOccurences("Yenapa"));
        assertEquals(1, df.getColumn("Note au BAC").getOccurences(13.11f));
    }

    /**
     * Test la copie avec sélecteur de colonnes
     */
    @Test
    void copyPartColumns(){
        final Object[][] expectedDataSet = new Object[][]{
            {"first_name","last_name","email","heureux?"},
            {"Padriac","M'Quharge","pmquharge0@google.com.hk",true},
            {"Joella","Larwell","jlarwell1@youku.com",true},
            {"Larina","Szubert","lszubert2@springer.com",false},
            {"Adela","Eagle","aeagle3@huffingtonpost.com",false},
            {"Jane","Michelle","jmichelle4@aol.com",false}
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);
        final DataFrame df = new DataFrame("file.csv");

        DataFrame part = df.getFirstRows(5).copyPartColumns(new String[] {"first_name","last_name","email","heureux?"});
        assertEquals(expectedDf, part);
        assertThrows(IllegalArgumentException.class, () -> part.copyPartColumns(new String[] {"first_name","mauvaisnom","email","heureux?"}));
    }

    /**
     * Test la copie avec sélecteur de lignes
     */
    @Test
    void copyPartRows(){
        final Object[][] expectedDataSet = new Object[][]{
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {2,"Joella","Larwell","jlarwell1@youku.com",33,true,15.28f},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
            {5,"Jane","Michelle","jmichelle4@aol.com",26,false,5.19f}
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);
        final DataFrame df = new DataFrame("file.csv");

        DataFrame part = df.getFirstRows(5).copyPartRows(new Integer[] {1,2,4});
        assertEquals(expectedDf, part);
        assertThrows(IllegalArgumentException.class, () -> part.copyPartRows(new Integer[] {-1}));
        assertThrows(IllegalArgumentException.class, () -> part.copyPartRows(new Integer[] {1000}));
    }

    /**
     * Test la copie avec sélecteur de lignes et de colonnes
     */
    @Test
    void copyPart(){
        final Object[][] expectedDataSet = new Object[][]{
            {"first_name","last_name","email","heureux?"},
            {"Joella","Larwell","jlarwell1@youku.com",true},
            {"Larina","Szubert","lszubert2@springer.com",false},
            {"Jane","Michelle","jmichelle4@aol.com",false}
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);
        final DataFrame df = new DataFrame("file.csv");

        DataFrame part = df.getFirstRows(5).copyPartRows(new Integer[] {1,2,4}).copyPartColumns(new String[] {"first_name","last_name","email","heureux?"});
        assertEquals(expectedDf, part);
    }


    /** 
     * Test sur le tri des valeurs
    */
    @Test
    void getOrderedValues(){
        final DataFrame df = new DataFrame("file.csv");

        //test sur type
        assertDoesNotThrow(() -> df.getColumn("first_name").orderByAscending());
        assertDoesNotThrow(() -> df.getColumn("first_name").orderByDescending());
        assertDoesNotThrow(() -> df.getColumn("age").orderByAscending());
        assertDoesNotThrow(() -> df.getColumn("age").orderByDescending());
        assertDoesNotThrow(() -> df.getColumn("heureux?").orderByAscending());
        assertDoesNotThrow(() -> df.getColumn("heureux?").orderByDescending());
        assertDoesNotThrow(() -> df.getColumn("Note au BAC").orderByAscending());
        assertDoesNotThrow(() -> df.getColumn("Note au BAC").orderByDescending());

        //test du tri
        assertEquals("Abbey", df.getColumn("first_name").orderByAscending().first());
        assertEquals("Zulema", df.getColumn("first_name").orderByAscending().last());
        assertEquals("Zulema", df.getColumn("first_name").orderByDescending().first());
        assertEquals("Abbey", df.getColumn("first_name").orderByDescending().last());

        assertEquals(1, df.getColumn("age").orderByAscending().first());
        assertEquals(100, df.getColumn("age").orderByAscending().last());
        assertEquals(100, df.getColumn("age").orderByDescending().first());
        assertEquals(1, df.getColumn("age").orderByDescending().last());
    }

    /**
     * Test la sélection avec bornes
     */
    @Test
    void testSelect(){
        final Object[][] expectedDataSet = {
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
            {4,"Adela","Eagle","aeagle3@huffingtonpost.com",18,false,11.19f},
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);
        final DataFrame df = new DataFrame("file.csv");
        final DataFrame dfE = df.getFirstRows(5);
        final DataFrame res = dfE.select("age", 17, 18);

        assertEquals(expectedDf, res);
    }

    /**
     * Test la sélection par un String
     */
    @Test
    void testSelectByString(){
        final Object[][] expectedDataSet = {
            {"id","first_name","last_name","email","age","heureux?", "Note au BAC"},
            {3,"Larina","Szubert","lszubert2@springer.com",17,false,12.14f},
        };
        final DataFrame expectedDf = new DataFrame(expectedDataSet);

        final DataFrame df = new DataFrame("file.csv");
        final DataFrame res = df.select("email","lszubert2@springer.com");

        assertEquals(expectedDf, res);
    }

}
